package com.cjy.springcloud.controller;

import com.cjy.springcloud.beans.Dept;
import com.cjy.springcloud.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DeptControllerFeign {

    @Autowired
    private DeptService deptService = null;

    @RequestMapping(value= "/consumer/dept/add" )
    public boolean add(Dept dept )
    {
        return deptService.add(dept);
    }

    @RequestMapping (value= "/consumer/dept/get/{id}")
    public Dept get(@PathVariable( "id" )Long id )
    {
        Dept dept = deptService.get(id);
        return dept;
    }

    @SuppressWarnings ( "unchecked" )
    @RequestMapping (value= "/consumer/dept/list" )
    public List<Dept> list()
    {
        return deptService.list();
    }

}
