package com.cjy.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

@EnableEurekaClient  //注册中心
@EnableFeignClients  //服务熔断
@SpringBootApplication
public class BootApplicationFeign {

    public static void main(String[] args) {
        SpringApplication.run(BootApplicationFeign.class,args);
    }

}
