package com.atguigu.springcloud.cfgbeans;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ConfigBean {

     //使用负载均衡--默认轮询操作
     @LoadBalanced
     @Bean
     public
    RestTemplate getRestTemplate() {
    return  new RestTemplate();
 }

    //负载均衡方式--随机调用
    @Bean
    public IRule myIRule(){
         return new RandomRule();
    }
}
