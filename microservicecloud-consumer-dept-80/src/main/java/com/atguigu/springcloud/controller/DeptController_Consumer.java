package com.atguigu.springcloud.controller;

import com.cjy.springcloud.beans.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
public class DeptController_Consumer {

    @Autowired
    RestTemplate restTemplate;
    //访问url的方式
    //private static final String REST_URL_PERFIX="http://localhost:8001";
    //访问服务名称的方式---不用关心地址与端口号
    private  static final String REST_URL_PERFIX = "http://MICROSERVICECLOUD-DEPT";


    @RequestMapping(value= "/consumer/dept/add" )
    public boolean add(Dept dept )
 {
 return restTemplate .postForObject( REST_URL_PERFIX + "/dept/add" , dept , Boolean. class );
 }

    @RequestMapping (value= "/consumer/dept/get/{id}")
    public Dept get(@PathVariable( "id" )Long id )
 {
     Dept dept = restTemplate.getForObject(REST_URL_PERFIX + "/dept/get/" + id, Dept.class);
     int i = 1+1;
     return dept;
 }

     @SuppressWarnings ( "unchecked" )
     @RequestMapping (value= "/consumer/dept/list" )
     public List<Dept> list()
 {
 return restTemplate .getForObject( REST_URL_PERFIX + "/dept/list" , List. class );
 }



}
