package com.atguigu.rule;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.AbstractLoadBalancerRule;
import com.netflix.loadbalancer.ILoadBalancer;
import com.netflix.loadbalancer.Server;

import java.util.List;
import java.util.Scanner;

public class Zdy_MyIRule extends AbstractLoadBalancerRule {
    private int total = 0; // 总共被调用的次数，目前要求每台被调用5次
    private int currentIndex = 0;

    public Server choose(ILoadBalancer lb, Object key){
        if(lb == null)
            return null;
        Server server=null;

        while(server == null){
            if(Thread.interrupted()){
                return null;
            }

            List<Server> upList = lb.getReachableServers();
            List<Server> allList = lb.getAllServers();
            int serverCount = allList.size();

            if(serverCount == 0)
                return null;

            if(total < 5){
                 server = upList.get(currentIndex);
                total++;
            }else{
                total=0;
                currentIndex++;
                if(currentIndex >= upList.size())
                currentIndex=0;

            }

            if (server == null){
                Thread.yield();
                continue;
            }

            if(server.isAlive()){
                return server;
            }

            server = null;
            Thread.yield();
        }
        return server;
    }

    @Override
    public void initWithNiwsConfig(IClientConfig iClientConfig) {

    }

    @Override
    public Server choose(Object key) {
        return choose(getLoadBalancer(),key);
    }

    public static void main(String[] args) {
      /*  short s = 128;
        byte b = (byte)128;
        System.out.println(b);
        String t="hello";
        char c []= {'h','e','1','1','0'};
        System.out.println(t.equals(c));
        System.out.println(c);*/
//        int [][]a=new int{3}[];

        /*for (int i =0;i<4;i++){
            new ThreadDemo().start();
        }*/


        /*Demo2 demo2 = new Demo2();
        new Thread(demo2).start();
        new Thread(demo2).start();*/

        Zdy_MyIRule zd = new Zdy_MyIRule();
        boolean res;
        do{
            res = zd.registry();
        }while(!res);
        System.out.println("欢迎您注册成功！！！");
    }



    public boolean registry(){
        boolean flag = true;

        System.out.println("欢迎登陆注册系统");
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入用户名");
        String username = scanner.next();
        System.out.println("请输入密码");
        String passwd1 = scanner.next();
        System.out.println("请再次输入密码");
        String passwd2 = scanner.next();
        if(username.length()<3){
            flag=false;
            System.out.println("用户名长度小于3");
        }
        if(passwd1.length()<6){
            flag=false;
            System.out.println("密码长度小于6");
        }
        if(!passwd1.equals(passwd2)){
            flag=false;
            System.out.println("两次输入密码不一致");
        }
        return flag;
    }

}
class ThreadDemo extends Thread{

    private static volatile int i=20;
    static String s = new String("ssss");


    @Override
    public void run(){

        while(i>0){
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (s){
                if(i>0) {
                    synchronized (s) {
                        System.out.println(Thread.currentThread().getName() + ":-------------------" + i--);
                    }
                }
            }
        }
    }

}

class Demo2 implements Runnable{

    private volatile int tick = 100;
    @Override
    public void run(){
        while(tick>0){
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (Object.class){
                if(tick>0){
                    synchronized (Object.class){
                        System.out.println(Thread.currentThread().getName()+"-------"+tick--);
                    }
                }
            }
        }
    }
}
