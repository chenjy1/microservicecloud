package com.cjy.springcloud.service;

import com.cjy.springcloud.beans.Dept;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

//标记代理哪个服务
@FeignClient(value = "MICROSERVICECLOUD-DEPT",fallbackFactory = DeptClientServiceFallbackFactory.class)
public interface DeptService {

    //注意因为是为服务方提供接口代理，所以下面是服务放的调用地址
    @RequestMapping(value= "/dept/add" )
    public boolean add(Dept dept );

    @RequestMapping (value= "/dept/get/{id}")
    public Dept get(@PathVariable( "id" )Long id );

    @RequestMapping (value= "/dept/list" )
    public List<Dept> list();
}
