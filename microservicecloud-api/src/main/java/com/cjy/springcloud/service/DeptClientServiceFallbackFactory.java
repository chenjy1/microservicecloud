package com.cjy.springcloud.service;

import com.cjy.springcloud.beans.Dept;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 服务降级：
 * 1.为服务接口提供服务调用失败的处理，类似与spring中的aop
 * 2.需要将降级处理类注入容器中
 * 3.测试：开启一个服务提供，开启一个服务消费
 *        测试正常调用，然后关闭服务提供，再次测试调用
 */
@Component //需要将此类注入容器中
public class DeptClientServiceFallbackFactory implements FallbackFactory<DeptService> {
    @Override
    public DeptService create(Throwable throwable) {
        return new DeptService(){

            @Override
            public boolean add(Dept dept) {
                return false;
            }

            @Override
            public Dept get(Long id) {
                return new Dept(id, "该ID：" + id + "没有没有对应的信息,Consumer客户端提供的降级信息,此刻服务Provider已经关闭", "no this database in MySQL" );
            }

            @Override
            public List<Dept> list() {
                return null;
            }
        };
    }
}
